##
import math
import sympy
from sage.all import *
from IPython.display import display
import itertools
##

def multiply_congruence_classes(n: int, class1: int, class2: int):
    for i in range(0, 10):
        for j in range(0, 10):
            print((class1 * i * class2 * j) % n)


def check_lin_diph_sol(a: int, b: int, particular: (int, int), homogenous: (int, int)):
    (xp, yp) = particular
    (xh, yh) = homogenous

    for k in range(-10, 10):
        print(a*(xp + xh*k) + b*(yp + yh*k))


def weird_fun(m: int, n: int, dir: bool, stop_m1=False) -> int:
    print((m, n))
    f = weird_fun

    step_x = lambda: f(m-1, n, dir) + 2*(m-1 + n)
    step_y = lambda: f(m, n-1, dir) + 2*(m + ((n-1) - 1))
    def result():
        if (m,n) == (1, 1):
            return 2
        if m == 1:
            return step_y()
        elif n == 1:
            return step_x()
        elif dir:
            return step_y()
        else:
            return step_x()
    r = result();
    return r

def partial_closed_form(m, n):
    return 2 * ((m-1)*m/2 + n*m - n)

def y_closed(n):
    return n*n - n + 2


def closed_form(m, n):
    # return n**2 - n + 2 + 2*((m-1)*m/2 + n*m - n)
    # return n**2 - n + 2 + m**2 -m + 2*n*m - 2*n
    return n**2 - 3*n + m**2 - m + 2*n*m + 2


##
def build_matrix(a, b):
    return sympy.Matrix([[1, a, -a, b], [0, 1, 0, b], [0, 0, 1, b], [0, 0, 0, 1]])

def q11_multiply_matrices():
    a1, b1, a2, b2 = sympy.symbols("a_1 b_1, a_2, b_2")
    m1 = build_matrix(a1, b1)
    m2 = build_matrix(a2, b2)
    return m1*m2

q11_multiply_matrices()
##

def q11_inverse():
    a, b = sympy.symbols("a b")
    m1 = build_matrix(a, b)
    return m1.inv()

q11_inverse()

## Q10a
##
(a,b,c,d) = sympy.symbols("a b c d")
m1 = sympy.Matrix([[0, 1], [1, 0]])
m2 = sympy.Matrix([[1, 0], [0, 1]])

display(m1*m1)

v = sympy.Matrix([[a, b], [c, d]])

print("g*m1:")
display(v*m1)
print("m1*g:")
display(m1*v)
print("Checking identity")
assert(v*m2 == v)
assert(m2*v == v)

##

# ----------------------- UTILITY FUNCTIONS --------------------------

def set_eq(s1, s2):
    if len(s1) != len(s2):
        return False
    for e1 in s1:
        if e1 not in s2:
            return False
    for e2 in s2:
        if e2 not in s1:
            return False
    return True

# Checks if the set `s` is closed under the binary operation `operation`
# by computing the product of all pairs of elements and ensuring that
# the result is the same as the original set
def ensure_is_closed(s, operation):
    unique_products = []
    for (l, r) in list(itertools.product(s, s)):
        lr = operation(l, r)
        rl = operation(r, l)
        if lr not in unique_products:
            unique_products.append(lr)
        if rl not in unique_products:
            unique_products.append(rl)

    if not set_eq(s, unique_products):
        display("s:")
        for e in s:
            display(e)
        display("products:")
        for e in unique_products:
            display(e)
        assert False

# Ensures that all inverses of elements in `s` are in the set `s`
def ensure_closed_inverses(s, inv_op):
    for e in s:
        if inv_op(e) not in s:
            display("Inv not in s", e)
            assert False

# If `sub` is not a normal group in `big`, return a witness of this fact, i.e. a
# case where an element from sub multiplied by an element in big from the left is not 
# the same as from the right
def non_normal_witness(sub, big, operation):
    for b in big:
        for s in sub:
            l = operation(s, b)
            r = operation(b, s)
            if l != r:
                return [l, r]
    return None

# -------------------- TESTS FOR UTILITY FUNCTIONS ----------------------------

def witness_test():
    (a,b,c,d) = sympy.symbols("a b c d")
    m1 = sympy.Matrix([[1, 0], [0, 1]])
    m2 = sympy.Matrix([[0, 1], [1, 0]])
    m3 = sympy.Matrix([[a, b], [c, d]])
    sub = [m1, m2]
    big = sub + [m3]
    witness = non_normal_witness(sub, big, lambda l, r: l*r)
    expected = [m2*m3, m3*m2]
    if witness != expected:
        display("w0: ", witness[0])
        display("w1: ", witness[1])
        display("e0: ", expected[0])
        display("e1: ", expected[1])
        assert False

try:
    ensure_closed_inverses([sympy.Matrix([[0, 0.5], [0.5, 0]])], lambda m: m.inv())
except AssertionError:
    print("Assertion thrown as expected")
else:
    assert False

witness_test()

# -------------------- END TEST ----------------------------

#q10b
##

print("b)")
# New group containing m0, m2 and now some mirroring matrices
original_mats = [
    m1,
    m2,
]
new_mats = [
    sympy.Matrix([[-1, 0], [0, -1]]),
    sympy.Matrix([[0, -1], [-1, 0]])
]

mats = original_mats + new_mats

ensure_is_closed(mats, lambda l, r: l*r)
ensure_closed_inverses(mats, lambda m: m.inv())

if witness := non_normal_witness(original_mats, new_mats, lambda l, r: l*r):
    display("Not normal with witness: ")
    display(witness[0], witness[1])

print("All tests passed! Normal :)")



##

# Q10c

def q10c():
    h1 = [
        sympy.Matrix([[1, 0], [0, 1]]),
        sympy.Matrix([[0, 1], [1, 0]])
    ]

    h2 = [
        sympy.Matrix([[1, 0], [0, 1]]),
        sympy.Matrix([[-1, 0], [0, -1]])
    ]
    g = [
        sympy.Matrix([[1, 0], [0, 1]]),
        sympy.Matrix([[0, 1], [1, 0]]),
        sympy.Matrix([[0, -1], [-1, 0]]),
        sympy.Matrix([[-1, 0], [0, -1]])
    ]
    op = lambda l, r: l*r
    inv = lambda m: m.inv()

    ensure_is_closed(h1, op)
    ensure_is_closed(h2, op)
    ensure_is_closed(g, op)
    ensure_closed_inverses(h1, inv)
    ensure_closed_inverses(h2, inv)
    ensure_closed_inverses(g, inv)

    assert non_normal_witness(h1, g, op) is None
    assert non_normal_witness(h2, g, op) is None

q10c()
