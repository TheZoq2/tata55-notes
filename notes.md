---
header-includes: |
    \usepackage{dsfont}
    \usepackage{upgreek}
---

# Lecture 2

## Binary operations

A function $*: X \rightarrow X \rightarrow X$ is called a binary operation, or a rule of composition.

Properties:
- **Commutative**: A binop $*$ is commutative if $x*y = y*x$
- **Associative** A binop $*$ is associative if $(x*y)*z = x*(y*x)$

## Function composition

The set of all maps $f: X \rightarrow X$ has a composition operation $f \circ g$ which performs $f(g(x))$

### Associativity
$$
(f \circ g) \circ h = (f \circ g)(h(x)) = f(g(h(x)))
$$
$$
f \circ (g \circ h) = f((g \circ h)(x)) = f(g(h(x)))
$$
$\Rightarrow$ Function composition is operation is associative

### Commutativity

$$
f \circ g = f(g(x))
$$
$$
g \circ f = g(f(x))
$$
is not commutative, for example $f = max(x, 10)$, $g = x-10$ means
$$f(g(1)) = max(1-10, 10) = 10$$
however,
$$g(f(1)) = 1-10 = -9$$

## Semigroups

A set $X$ with a binary operation $\circ$, $(X, \circ)$ is called a semigroup if
$\circ$ is associative.

## Monoids

A semigroup with an additional identity element $e$ such that 
$$
x \circ e = e \circ x = x
$$
is a monoid.

**Theorem**: The identity element is unique if it exists.

**Proof**: assume two different identity elements $e_1$ and $e_2$, then

$$
e_1 = e_1 \circ e_2 = e_2
$$

## Semigroup and monoid examples

- All words on an alphabet M form a semigroup under concatenation. This is called the **free semigroup**.
- The free semigroup augmented with the *empty word* form a monoid. the **free monoid**.
- All monomials in $M$ i.e. all words where symbol order is irrelevant. $aaab = abaa = a^3b$ is a commutative monoid. The **free commutative monoid**.
- $2 \times 2$ matricies under multiplication is a monoid
- $X^X$, all maps from $X$ to itself under composition is a **monoid**.

## Multiplication table

A binary operation can form a multiplication table where the left hand operand
is on the left edge, and the right hand operand at the top edge. For
multiplication, this would be drawn as 

```
   | 1 | 2 | 3  | 4
---+---+---+---+----
1: | 1 | 2 | 3  | 4
2: | 2 | 4 | 6  | 8
3: | 3 | 6 | 9  | 12
4: | 4 | 8 | 12 | 16
```

## Groups

A group is a *monoid* $(X, \circ, e)$ where each element $x \in X$ has a (necessarily unique) two sided inverse $x^-1$ such that
$$
x \circ x^{-1} = x^{-1} \circ x = e
$$

A group where $\circ$ is commutative is a commutative group, also known as an **Abelian group**

**Theorem**: for any group $(G, \circ, e)$, the inverse of an element $g \in G$ is unique

**Proof**: Assume $h$ and $k$ are inverse elements of $g$, then
$$
h = h \circ e = h \circ (g \circ k) \overset{associativity}{=} (h \circ g) \circ k = e \circ k = k
$$

## The Symetric Group $S_X$

All invertible maps $f : X \rightarrow X$ form a group called the **symetric group** $S_X$

## Some lemmas

Each row and column in a multiplication table of a group is a permutation of
the members of the set

**Cancellation lemma**: If $g, h, k \in H$, and $hg = kg$ then $h = k$.

**Proof**:
$$
hg = kg \Leftrightarrow (hg)g^{-1} = (kg)g^{-1} \Leftrightarrow k(gg^{-1}) = h(gg^{-1}) = k = h
$$
**Why does this work?** Why can we extend both sides by $g^{-1}$?

**Linear equation uniqueness**: For $a, b \in G$ equation $ax = b$ has the unique solution $x = a^{-1} b$

**Proof**: $a^{-1}ax = a^{-1}b = x$.

**Note**: How does this show uniqueness?

## Subgroups

For a group $G = (G, *, e)$, a subset $H \subseteq G$ is a subgrop, denoted $G \leq H$ if

- $e \in H$: The identity element is contained in H
- $a, b \in H \Rightarrow a*b \in H$. For any two elements in $H$, the product should be in $H$
- $a \in H \Rightarrow a^{-1} \in H$. The inverse of any element in $H$ should be in $H$

Equivalently, if $H$ is a group with the same operation as $G$, then $H \leq G$

$G$ is a subgroup of itself, as is the group $(\langle e\rangle, *, e)$. The set of subgroups
not containing these are proper subgroups (or is the identity group included there??).

## Lemmas

- If $H \leq K \leq G$ then $H \leq G$ (Subrgoupness is transitive)
- If $H \leq G$ and $K \leq G$ then $H \cap K \leq G$
- If $S$ is a subset of $G$ then the intersection of all subgroups of $G$ that contain $S$
is a subgroup denoted by $\langle S \rangle$. It is the unique smallest subgroup that contains $S$


# Lecture 3

$[a]_n$ is all numbers congruent to $a$ mod $n$.

- $[0]_3 = \{0, 3, 6, ...\}$
- $[1]_3 = \{1, 4, 7, ...\}$
- $[2]_3 = \{2, 5, 8, ...\}$

In $\mathbb{Z}$, $g=[a]_n$ has a multiplicative inverse iff $\text{gcd}(a,n) = 1$

The class $U_n$ is the set of all congruence classes with this property. I.e.
$$
U_n = \{[a]_n | gcd(a, n) = 1\}
$$

The unit circle $\mathfrak{T}$ under multiplication forms a subgroup under the punctured complex plane $C^*$.

## Exponentiation

For a group $(G, \circ, e)$ and an element $g \in G$

- $g^0 = e$
- $g^2 = g \circ g$, $g^3 = g \circ g \circ g$. Unambiguous thanks to associativity
- Also works for inverses $g^{-2}$ = $g^{-1} \circ g^{-1}$. $g^{-n} = (g^{n})^{-1} = (g^{-1})^n$

**Lemma**: For all $i, j \in \mathbb{Z}$, $g^{i} \circ g^{j} = g^{i+j}$

## Element order

An element $g$ has order $n$ denoted $o(g) = n$ if
$$
g^n = e
$$
but $g^m \neq 1$ for $1 \leq m < n$. $n$ is the smallest number such that $g^n$ is 1

The order is infinite if $g^n \neq 1$ for all $n > 0$


## Cyclic subgroups

For an element $g$ of a group $(G, \circ, e)$, the **cyclic subgroup** generated by $g$ is denoted by
$$
\langle \{g\} \rangle = \{g^n | n \in \mathbb{Z} \}
$$
generally, using the shorthand $\langle g \rangle$

The cyclic group $\langle g \rangle$ is the smallest subgroup that contains $g$

A group G is cyclic if it has a generator s.t.

$$
G = \langle g \rangle
$$

### Abelian groups and addition

If $G$ is abelian it is often written as $(G, +, e)$ with $e=0$. Then notation is addition-like rather than multiplication

- $g^n$ is $ng$
- $g^0$ is $0g$
- $\langle g \rangle = \mathbb{Z}g = \{ng | n \in \mathbb{Z}\}$


$\{\mathbb{Z}, +, 0\}$ is an infinite cyclic group generated by $1$ or $-1$

For any $n > 2$, $\mathbb{Z}_n$ is a finite cyclic group generated by $[1]_n$,
and by any $[a]_n \in U_n$

## Isomorphism

An isomorphism between groups $G$, $H$ is a bijection $\upphi : G \rightarrow H$ where
$\upphi(x \circ_G y) = \upphi(x) \circ_H \upphi(y)$

Up to re-labeling of elements, two isomorphic groups are the same.

## Cyclic group isomorphism

**Theorem** If $G$ is finite then it is isomorphic to $Z_n$, and if it is infinite, it is isomorphic to $\mathbb{Z}_n$

## The infinite cyclic group

To avoid additive notation, we introduce $C_{\inf}$, the infinite cyclic (multiplicative) group, and the cyclic group of order $n$ $C_{n}$

For additive-like groups, we use $\mathbb{Z}_n$ and for multiplicative-like groups, $C_n$

## Subgroups of cyclic groups

The subgroups of the cyclic group $G = \langle g \rangle$ are all cyclic. If
$G$ is infinite, the subroups, excluding $\langle g^0 \rangle$ are infinite and
hence isomorphic to $G$ itself.

If $G$ is finite, i.e. $|G| = n$, then $H = G$ whenever $gcd(k, n) = 1$. That is, the
greatest common divisor of the size of $n$ and $k$, the power of $g$ is 1. Otherwise the
size of the subgroup is

$$
|H| = \frac{n}{gcd(k, n)}
$$

## Direct products of subgroups

For groups $G, H$, the direct product $G \times H$ is a group with the operation

$$
(g_1, h_1) \circ (g_2, h_2) = (g_1 \circ_G g2, h_1 \circ_H h_2)
$$
the identity element
$$
(1_G, 1_H)
$$
and inverse
$$
(g,h)^{-1} = (g^{-1}, h^{-1})
$$

### Extended to more groups

$$
(G \times H) \times K \simeq G \times (H \times K) 
$$
which means it can be denoted by $G \times K \times H$

The direct product, $G \times G$, $G \times G \times G$... are denoted
$$
G \times G = G^2
$$

## Size of products

If for $g \in G$, $h \in H$, $o(g) = m$, $o(h) = n$ and $m,n$ are finite, then the order of $(g, h) \in G \times H$ is $lcm(m, n)$

## Cyclic products

For positive $m, n$, then $C_m \times C_n$ is cyclic iff $gcd(m, n) = 1$


# Lecture 4

## Bijections

A function $f : X \rightarrow Y$ is a bijection if there is a one to one
mapping between $X$ and $Y$ and it is invertible

## The symetric group

For a set $X$, the symmetric group $S_X$ is the set of bijections $f : X \rightarrow X$ under functional composition.

## Permutations
The symmetric groups $S_n$ of positive integers $0..n$ are permutations.

Permutations are usually denoted by $\sigma$ and can be described in too many ways:

- As bipartite graphs
- As directed graphs
- On 2 row notation
$$
\begin{bmatrix}
    1 & 2 & 3 \\
    2 & 1 & 3
\end{bmatrix}
$$
- On 1 row notation where the order is implicit
$$
\begin{bmatrix}
    2 & 1 & 3
\end{bmatrix}
$$
- On cycle notation
    $$
        (1, 2)(3)
    $$

## Conjugation

With $X$, $Y$ being sets, and $\phi : X \rightarrow Y$ being a bijection, the maps
$$
S_X \in f \mapsto \phi \circ f \circ \phi^{-1} \ni S_Y
$$
$$
S_Y \in g \mapsto \phi^{-1} \circ g \circ \phi \ni S_Y
$$
are inverses of each other which means they are bijections

Applying $f$ in $S_X$ is the same as applying $\phi \circ f \circ \phi^{-1}$ in $S_Y$

$f$ and $\tilde{f} = \phi \circ f \circ \phi^{-1}$ are conjugate.

## Isomorphism to the symetric group

If $X$ is a set of $n$ elements, then $S_X$ is isomorphic to $S_n$ ($S_X \simeq S_n$)

**Proof** Number the elements in $X$ to get a bijection $\phi [n] \rightarrow X$. This gives
the desired isomorphism as the conjugation $f \mapsto \phi^{-1} \circ f \circ \phi$

## Permutations as matrices

A permutation can be represented as a matrix $M_\sigma$ in a "rook
configuration". Function composition is then isomorphic(?) to matrix
multiplication

## Cycles and cycle types

In the directed graph representation of a permutation, each node has in-degree
and out-degree one. Hence, it consists of a number of directed cycles.

The cycle notation of a permutation $\sigma$ is a list of the elements in each cycle
$$
(a_1, a_2, a_{l-1})(a_l .. a_{m-1}) ... ()
$$
where single-node cycles are usually omitted

The **cycle type** of a permutation is the length of each cycle, usually denoted
as a tuple of descending lengths, $(3, 2, 2, 1)$

**Theorem**: Two permutations $\sigma, \tau \in S_n$ are conjugate iff the have
the same cycle type


*Note*: $a, b$ are conjugate if there exists a $c$ such that $a = c b c^{-1}$

## Permutation statistics

### Descents
A descent is an index $i$ such that $\sigma(i) > \sigma(i+1)$. The set
$\textbf{Des}(\sigma) \subseteq [n-1]$ is the set of descents (by where they start?), and
$\textbf{des}(\sigma)$ is the number of descents.

The **major index** $\textbf{maj}$ is the sum of the elements in $\textbf{Des}{\sigma}$

### Inversions

An inversion is a set of indices $i, j$, $i < j$ such that $\sigma(i) >
\sigma(j)$. $\textbf{Inv}(\sigma) \subseteq [n]^2$ is the set of indices which
form inversions, and $\textbf{inv}(\sigma)$ is the number of inversions


## Function composition notation

*We* denote a function applied to $x$ as $f(x)$, and $(g \circ f)(x) =
g(f(x))$.

If $f, g \in S_n$ then $fg \in S_n$ is the bijection of first applying $f$,
then applying $g$ on the result.

Others denote $(x)f$ or just $xf$ in which case $fg$ means applying f first.

## Transpositions

A $k$ cycle is a permutation with a single cycle of length $k$, and the rest
being length $1$

A $2$-cycle is called a **transposition**


## Involutions

An **involution** is a permutation whose square is the identity

## Disjoint cycle factorisation

A permutation can be factored into disjoint cycles. For example, if $f =
(1,2,3)(4,5,6)$ then f = gh where $g = (1, 2, 3)(4)(5)(6)$ and
$h=(1)(2)(3)(4,5,6)$.

**Lemma** Disjoint cycles commute.

**Lemma** Transpositions are involutions

**Lemma** Involutions are products of disjoint transposition and hence have
cycle type (2, 2, 2,... 1,...,1)

**Lemma** A k-cycle has order $k$

**Lemma** The order of a permutation is the least common multiple of the size
of its cycles

## Transposition decomposition
Any $k$-cycle can be written as a product of transposition

Every transposition is the product of an odd number of adjacent transpositions, i.e. $(j, j+1)()$


# Lecture 5

## Equivalence relations and congruences

An equivalence relation $\sim$ on a semigroup $S$ can be a left congruence,
right congruence or congruence if for all $a,s,t \in S$

- Left congruence: $s \sim t$ implies $as \sim at$
- Right congruence: $s \sim t$ implies $sa \sim ta$
- Congruence: $s \sim t$ and $a \sim b$ $sa \sim tb$

## Equivalence classes

For a set $X$ and a relation $\sim$ where $\sim$ is reflexive, symmetric and
transitive, it gives rise to a set of equivalence classes

$$
[x]_{\sim} = \{y \in X: y \sim x \}
$$

These classes are disjoint and the union of them make up $X$

## Quotient class

All equivalence classes of a set under an equivalence relation make up a quotient class

$$
X/_\sim = \{[x]_\sim : x \in X\}
$$

### Canonical Quotient Surjection

**A canonical quotient surjection** is a surjection which maps an element to its
equivalence class

$$
\pi : x \rightarrow X/_\sim
$$
$$
\pi = x \mapsto [x]_\sim
$$


## Normal Subgroups

A subgroup $H \leq G$ is *normal in $G$* if $ghg^{-1} \in H$ for each $h \in H$, $g \in G$

Assuming $G$ is a group, $\sim$ is a congruence, then $N = [1_{g}]_\sim$

**Theorem**: $N \triangleleft G$

## "Multiplication of groups"

Given 2 subsets of G $A, B \subseteq G$, we can write

$$
AB = \{ ab : a  \in A, b \in B \}
$$

For multiplying an element by a group, we write the shorthand

$$
\{a\}B = aB
$$

## Cosets

For a subgroup $H$ $gH = \{gh : h \in H\}$ is a left coset, while $Hg$ is a
right coset with representative $g$

In a commutative group, left cosets are identical to right cosets, but this is
not true for all groups


## Theorems on cosets

The **index** of $H$ in $G$ is the number of cosets of $H$ in $G$, denoted by $[G:H]$.

The left cosets of $H$ in $G$ partition $G$

The size of the left cosets is the same as the size of the right cosets


## Lagrange's theorem

Left cosets are equipotent to H, that is, $|gH| = |H| \forall g$. All cosets
have the same cardinality

This means that the size of cosets has to be be a divisor of the size of the super group 

It also means that the order of $g$ must divide $|G|$, $o(g) | |G|$

A resulting theorem is that $\frac{|G|}{|H|} = [G:H]$

## Homomorphisms

For semi-groups $S$ and $T$, a semigroup homomorphism is a function $f : S
\rightarrow T$ such that $f(x * y) = f(x) * f(y) \forall x,y \in S$

The image $Im(f)$ is the mapped result of all elements in $S$, i.e. $Im(f) =
\{f(g) : g \in S\}$

The kernel (of a subgroup?) $ker(f)$ is the set of tuples $(g_1, g_2)$ where $f(g_1) = f(g_2)$

The image of a semigroup is a subsemigroup of $H$, and the kernel is a congruence on $G$

For **groups** $G$, $H$ and a homomorphism $\phi$, the important thing seems to
be that $\textbf{ker}\phi$ is all the elements in $G$ which map to identity in
$H$

A kernel is a normal subgroup

**Question** How is the (g1, g2) thing related to the inverse mapping?

## Quotient structures

Given a surjective group homomorpism $\phi : G \rightarrow H$ for groups $G$
and $H$ with a $\textbf{ker}\phi = N$ and associated congruence $\sim$

The quotient $S/\sim$ = $S/N$ is the set of left or right cosets of $N$

## First isomorphism theorem

The image of a group homomorphism with kernel $N$ is isomorphic to $G/N$

$$
G/N \simeq \textbf{Im}(\phi)
$$

## Correspondence theorem

For a group $G$ and a normal subgroup $N$

Every subgroup of the quotient gruop $G/N$ is on the form $H/N$ where H is a subgroup of $G$ and $N$ is a subgroup of $H$.

Each subgroup of G has a corresponding (smaller?) subgroup in $G/N$ which is
the quotient group $H/N$


# Lecture 7

**NOTE**: for the rest of this lecture groups are assumed to be abelian

## Direct products

For a set of groups $G_1 .. G_r$, the direct product $\times$: $G_1 \times ... \times G_r = (g_1, ... g_r: g_1 \in G_1 .. g_r \in G_r)$

## Internal products

G is the internal direct product of $H_1..H_k \leq G$ if $G \simeq H_1 \times \dots \times H_k$. I.e. I.e. if G is isomorphic to the direct product of the subgroups $H_1..H_k$

**QUESTION** What does slide 6 of lecture 7 mean?

## Torsion subgroup

The torsion subgroup of $G$ is the set of all elements of $G$ of finite order

## P-torsion

For a prime number $p$, the p-torsion subgroup of $G$ is

$$
G[p] = \{g \in G : o(g) = p^a for some whole number a\}
$$

I.e. the elements in $G$ which are of order $p^a$

## P-group

A group $G$ is a p-group if $G = G[p]$, that is all elements in $g$ are of order $p^a$

**Lemma**: if $G$ is a finite group, then $G$ is a p-group if and only if the
size of $G$ is $p^a$ for some a

## P-group isomorphism

$G$ is isomorphic to the direct product of its p-torsion subgroups for all primes $p$ i.e.

$$
G \simeq G[2] \times G[3] \times G[5] ...
$$


# Lecture 8

## Group actions

A group action is a map from group elements to functions on sets, i.e.

$$
\phi G \rightarrow S_x
$$
where $S_x$ is functions on the set $X$

$\phi(g)$ is then a function $X \rightarrow X$ and when wanting to denote it applied to a set element $x$, we can write
$$
\phi(g)(x)
$$
however, we usually use the short hand notation
$$
g.x
$$

Each group action is a bijection on the set, and in the finite case, a permutation.

### Properties

A group action is the same as a map
$$
G \times X \rightarrow X
$$
$$
(g,x) \mapsto g.x
$$

For all $g$ and $h$ in $G$ and $x$ in $X$:

The identity element in the group maps x to itself
$$
1.x = x
$$
multiplying two group elements and taking the group action is the same as applying
the group elements sequentially
$$
(gh).x = g.(h.x)
$$

### Right actions

The above is a left action. We can also have right actions where functions are applied in the opposite order, i.e.
$x.(gh) = (x.g).h$

## Fixed points

The fixed points of a group element $g$ $\textbf{Fix}(g)$ are the set members
$x$ where $g.x = x$

## Stabilizers

The stabilizer $\textbf{Stab}(x)$ of a set element $x$ is the subset of group
elements $u \in G$ where $u.x = x$

A stabilizer is a subgroup of $G$

It is not normal because the left and right cosets differ

The index of a stabilizer, i.e. is the size of its orbit, $[G: \mathbf{Stab}(x)] = |\mathbf{Orb}(x)|$

## Orbit

The orbit of a set element $x$ is the subset of elements $z$ of $X$ for which
there is a group element $g$ which maps $z$ to $x$

## G-equivalency

Two elemnts of a set are G-equivalent if there is a group element that maps one
to the other. It is an equivalence relation on $X$ with the equivalence classes
being the orbits in the set.


A ring is an abelian group (R, +, 0) augmented with an associative multiplication
which satisfies the distributive laws

Rings don't necessarily have a multiplicative unit $1$ but if they do, they are
called *unitary*.

A ring is *commutative* if the multiplication is commutative. The addition is always
commutative because of the abelianness

## Zero divisors

An element $r$ is z a zero divisor if $rs = 0$ or $sr = 0$ for some non-zero $r$

## Unit

An element $r$ is a unit if there is an element $s$ such that $sr = rs = 1$ for all $r$

### Unitary ring

A unitary ring $R^*$, sometimes $\mathcal{U}$ is a group under multiplication. It is the *multiplicative group* of $R$

## Nilpotency

$r$ is nilpotent if $r^n = 0$ for some $n$

## Idemopotency

If $r^2 = r$. Idem means same, so this is the same under potentiation


## Properties of commutative unitary rings

- The set of idempotent elements is closed under multiplication
- The set of nilpotent elements is closed under multiplication and addition and
  is absorbing. The product of an element and a nilpotent element is nilpotent
- Zero divisors are closed under multiplication

## Division ring

A unitary ring $R$ is a division ring if $R^* \cup \{0\} = R$

A commutative division ring is a *field*, and a non-commutative division ring
is a *skew field*


## Fields

Fields have addition, subtraction, multiplication *and* division which behave as on the real numbers

# Lecture 12

A polynomial ring $L[x]$ is the set of maps $c : \mathbb{N} -> L$. Or more
easily understandable: all polynomials with coefficients in $L$ where $L$ is a
unitary, commutative ring.

$L[x]$ it self is commutative and unitary

The map $c$ maps powers of $x$ to their coefficients, i.e. $c(5) = 3$ means the polynomial contains the term $4c^5$

## theorem: Polynomial domainness

A polynomial ring $L[x]$ is a domain iff $L$ is a domain, i.e. the ring of coefficients
is a domain.

(A domain has no zero divisors except 0)


## theorem: Polynomial invertability

A polynomial $f \in L[x] = a_0 + a_1 x + ... a_n x^n$ is invertible iff $a_0$ is a unit (has a multiplicative inverse) in $L$ and $a_1..$ are nilpotent ($a_k ^ m = 0$ for some $m$)

## lemma: Nilpotency addition

If $x$ and $y$ are nilpotent, so is $x+y$

## lemma: Unity addition

If $x$ is a unit and $y$ is nilpotent, then $x+y$ is a unit

## Leading things

In a polynomial, the leading term is $a_n x^n$, the leading monomial is $x^n$
and the leading coefficient is $a_n$ where $n$ is the largest power present in
the polynomial.

The degree $deg(x)$ of a polynomial is $n$

## Polynomial ring degree

For 2 polynomials $f,g \in L[x]$

- $\mathbf{deg}(f+g) = max(\mathbf{deg}(f), \mathbf{deg}(g))$
- $\mathbf{deg}(fg) \leq \mathbf{deg}(f) + \mathbf{deg}(g)$

If $L$ is a domain

- $\mathbf{deg}(fg) = \mathbf{deg}(f) + \mathbf{deg}(g)$
- $\mathbf{lt}(fg) = \mathbf{lt}(f)\mathbf{lt}(g)$ (leading term(?))

## Evaluation of polynomials

This is just like normal application of a polynomial, replace $x$ with $u$ where
$u \in L$

Polynomial evaluation is a ring homomorphism, it maps $L[x] -> L$

The set of polynomials which evaluate to 0 for $u$ ($I_u = {f(x) \in L[x] | f(u)=0}$) is an ideal and

$$
\frac{L[x]}{I_u} \simeq L
$$


## Coefficients in a domain

- (A domain has no zero divisors except 0)
- (if $L$ is a domain $L[x]$ is as well)
- ($f$ is invertible iff $a_0$ is a unit, and $\mathbf{deg}(f) = 0$)

Given 2 polynomials $f, g \in L[x]$

- $f|g$ if there exists $h$ s.t. $g = fh$
- $f, g$ are **associate** ($f \sim g$) if $f|g$ and $g|h$. Analogous to $3 \sim -3$
- $f$ is irreducible if it lacks non-trivial divisors. (Any divisor of $f$ is either associate to $f$ or a unit)
- $f$ is prime if $f|gh$ implies $f|g$ or $f|h$

### Divisibility

- $f|g$ iff $(f) \supseteq (g)$
- $f ~ g$ iff $f = cg$ where $c$ is a unit
- A non-trivial divisor $g$ of $f$ has smaller degree than $f$ and the degree is non-zero. $0 < \mathbf{deg}(g) < \mathbf{deg}(f)$
- A prime element is irreducible

## Multi-variable polynomial rings

$L[x,y]$ = $(L[x])[y]$



## Coefficients in a field

Given 2 polynomials $f(x)$, $g(x)$ there is a unique quiotient and remainder
that works akin to the integer case, i.e.
$$
f(x) = a(x)g(x) + r(x)
$$
where $\mathbf{deg}(r(x)) < \mathbf{deg}(g(x))$

## GCD

A generator of a principal ideal $(f) + (g)$ is a gcd of $f$ and $g$

If $h = gcd(f, g)$ then $h|f$, $h|g$ and if $h'|f$, $h'|g$ then $h'|h$

## Euclidean algorithm

If $f=ag + r$ then $gcd(f, g) = gcd(g, r)$, analogous to integer euclidean algo.

## Factor theorem

With $f(x) \in K[x]$ and $a \in K$, then $a$ is a zero of the polynomial $f$, i.e. $f(a) = 0$ iff $(x-a)|f(x)$

## Ideal theorems

With $I = (f) \subseteq K[x]$

- $I$ is a prime ideal if $f$ is the zero polynomial or if $f$ is irreducible
- $I$ is maximal iff $f$ is a non-zero irreducible polynomial

## Unique factorization

A non-zero polynomial in $f \in K[x]$ can be written as a product of irreducible polynomials

The factorization is unique up to ordering and associate factors

### Irreducibility degrees

- In $\mathbb{C}[x]$ irreducible polynomials have degree 1
- In $\mathbb{R}[x]$ irreducible polynomials have degree 1 or 2
- In $\mathbb{Q}[x]$, $\mathbb{Z}_p[x]$ irreducible polynomials have any degree
- In $F[x]$ where $F$ is a finite field, irreducible polynomials have any degree

## Quotients

With $f = a_0 + a_1 x + ... + a_n x^n \in K[x]$ where $\mathbf{deg}(f) = n > 0$ and $I =(f)$, i.e. I is generated by $f$. Then $R = K[x]/I$

- R is a domain iff it is a field iff it is irreducible
- R is a K-vector space of dimension $n$. A natural basis is $1, \bar{x}, ... \bar{x}^{n-1}$


# Lecture 13

## Fields of fractions

For an integral domain D:

There is an injective ring homomorphism $\eta : D \rightarrow F$ where $F$ is a field such that *any* injective ring homomorhism $F : D \rightarrow K$ to a field $K$ factors through $F$ as $f = \hat{f} \circ \eta$

### Fraction field

When $D = K[x]$ then the *fraction field*
$$
F = K(x) = \left\{ \frac{f(x)}{g(x)} | f(x),g(x) \in K[x]\text{ and }g(x) \neq \text{zero polynomial} \right\}
$$
is called the \textit{field of rational functions}


### Smallest subdomains

Any domain $D$ contains a smallest subdomain, it is either an isomorphic copy of $\mathbb{Z}$ or $\mathbb{Z}_p$.

Any field contains a smallest subfield which is either $\mathbb{Q}$ or $\mathbb{Z}_p$

## Divisibility in domains

Given $u, v, w \in D \ {0}$, i.e. 3 elements in a domain, which are not $0$

- If $u|1$, then u is a unit. I.e. only only a unit divides 1
- If $u|v$ and $v|u$ then $u=cv$ where $c$ is a unit, and $u \sim v$
- If $w|u$ and $w|v$, then $w$ is a common divisor of $u$ and $v$. It is a
  greatest common divisor if $w$ is divisible by any other common divisor
- GCD is defined inductively for more than one argument, i.e.
  $\text{gcd}(a,b,c) = \text{gcd}(\text{gcd}(a,b),c)$
- $w$ is irreducible if any divisor is either a unit or associate to $w$
- $w$ is a prime element if $w|uv$ implies $w|u$ or $w|v$
- The domain $D$ has finite factorization if all non-zero elements are finite
  products of irreducible elements
- D is a *unique factorization domain* (UFD) if it has finite factorization,
  and that factorization is unique, up to order and associates

## Euclidean domains

A domain D is euclidean if there is a function $d : D \rightarrow \mathbb{N} \cup {-\infty}$ such that

- $d(u+v) \leq \text{max}(d(u), d(v))$
- $d(uv) \leq d(u) + d(v)$
- $d(0) = -\infty$

This function should work for a division algorithm, i.e. for $u, v \in D$, $v \neq 0$, there are unique $k, r$ s.t.
$$
u = kv + r, d(r) < d(v)
$$

### Examples
- $\mathbb{Z}$ is a euclidean domain with $d(u) = |u|$
- $K[x]$ is a euclidean domain with $d(u) = deg(u)$
- The gaussian integers $Z[i] = {a+ib | a,b}$ with $a,b$ being integers is an
  euclidean domain with $d(a+ib) = a^2 + b^2$

### Properties
Euclidean domains have an euclidean algorithm which means that $gcd$s exist and
Bezout's theorem holds

Euclidean domains are principal ideals

## Finite factorization

If a domain D has finite factorization, and irreducible elements are prime, then it is a UFD.

## Principal Ideal Domains (PID)

In a PID, $(u)$ is maximal iff $u$ is irreducible

In a PID, irreducible elments are  prime

Any PID is a UFD

Any PID has finite factorization

Any euclidean domain is a UFD

If a domain $D$ is a UFD, then so is $D[x]$

In a PID, strictly increasing chains of ideals eventually stablilize, i.e. $I_n = I_{n+1}$
$$
I_1 \subsetneq I_2 \subsetneq I_3 ...
$$

## $\mathbb{Q}[x]$ and $\mathbb{Z}[x]$

A polynomial
$$
f(x) = \sum_{j=0}^{n} a_j x^j \in \mathbb{Z}[x]
$$
is irreducible iff it is reducible when viewed as a polynomial in $\mathbb{Q}[x]$

### Linear factors

If f(x) has a rational zero $\frac{r}{s}$ with $gcd(r,s) = 1$, then $r|a_0$ and
$s|a_n$

# Lecture 14

## General field extensions

Given 2 fields $E$ and $F$ where $E$ is a subring of $F$, i.e. $E \leq F$. We
say that E is a **subfield** of $F$ and that $F$ is an **overfield** of $E$.

The **inclusion map** $i : E \rightarrow F$ is called a field extension

### Degree

Given a field extension $E \leq F$, then $F$ is a vector space over $E$ The
dimension is denoted by $[F:E]$ and is called the degree.
The extension is finite if the degree is.

### Tower theorem

If $K \leq L \leq M$ then $[M:K] = [M:L][L:K]$

## Algebraic extensions

If $E \leq F$ is a field extension, then $u \in F$ is algebraic over $E$ if
there is a non-zero polynomial $f(x) \in E[x]$ which has $u$ as a zero, i.e.
$$
f(x) \sum a_i x^i \text{where} a_i \in E,
$$
and
$$
f(u) \sum a_i u^i = 0 \in F,
$$

The smallest degree of a polynomial that works is the degree of $u$ over $E$

A non-algebraic $u$ is transcendental over $E$

An *extension* $E \leq F$ is algebraic if every element in $F$ is algebraic over $E$

### Theorem

If the degree of an exteinsion $[F:E] = n < \leq \infty$ then $E \leq F$ is algebraic

## Simple extensions

If $E \leq F$ is a field extension with $u \in F$. Then $E(u)$ denotes the
smallest subfield of $F$ containing $E$ and $u$. $E(u)$ is a simple extension
and $u$ is a primitive element of the extension.

### Classification

With $E \leq F$ a field extension and $u \in F$

If $u$ is algebraic over $E$ and of degree $n$, then $E \leq E(u)$ is algebraic, and
$$
E(u) \simeq \frac{E[x]}{p(x)}
$$
where $p(x)$ is irreducible, has degree $n$ and is unique (up to association), non-zero and of the smallest degree s.t. $p(u) = 0$.

### Iterated simple extensions

Given $u_1, u_2, u_3$ in $F$ of a field extension $E$, we define $E(u_1, u_2,
...)$ as the smallest extension of $E$ inside $F$ which contains all $u$, or
repeated extension, i.e. $E(u_1)(u_2)...$


### Extension finiteness

An extension $E \leq F$ is finite dimensional iff there is a finite number of
elements $u_1... \in F$ which are algebraic over $E$, such that $F = E(u_1...)$

### Primitive element theorem

With a finite dimensional extension $E \leq F$ where $char(e) = 0$ (i.e. $Q
\leq E$) or $E$ is finite, then there is a primitive element $u \in F$ for the
extension $F = E(u)$


## Zeros of polynomials

Let $E \leq F$ be a field extension and
$$
f(x) = \sum_{i=0}^{n} a_i x^i \in E[x]
$$

This might not have zeros in $E$, but
$u \in F$ is a zero of $f$ if
$$
f(u) = \sum_{i=0}^{n} a_i u^i = 0 \in F
$$

$u$ is a simple zero if $(x-u)|f(x)$ but $(x-u)^2 \nmid f(x)$

It is a zero of multiplicity $r$ if $(x-u)^r | f(x)$ but $(x-u)^{r+1} \nmid f(x)$

## Kronecker's theorem

Let $f(x)$ be a non-constant polynomial in $E[x]$, then $f(x)$ has a zero somewhere


## Splitting field

A polynomial
$$
f(x) = \sum_{i=0}^{n} a_i x^i \in E[x]
$$
splits over an extension F if there are distinct zeros s.t.
$$
f(x) = a^n \prod_{j=1}^{r}(x-u_j)^{b_j}
$$

The extension that gives the splitting field is the extension of $E$ with all
the zeros of the polynomial, and all consequences of adding them, i.e.
$E(u_1, ..., u_n)$


The degree $[F:E] \leq n!$  and if $f(x)$ is irreducible, then the degree is greater than or equal to $n$, i.e. $[F:E] \geq n$

## Algebraic closure

A field $\bar{K}$ is an algebraic closure of $K$ if
- $K \leq \bar{K}$
- The extension is algebraic
- Every polynomial $f(X) \in K[x]$ splits over $\bar{K}[X]$

A field $E$ is algebraically closed if every non-constant polynomial in $E[x]$
has a zero in $E$

If $E$ is algebraically closed and $f(x) \in E[x]$, then $f(x)$ splits in $E$.

An algebraic closure of $\bar{K}$ of $K$ is algebraically closed.

For a field $E$ there exists a unique (up to rigid isomorphism) algebraic closure $\bar{E}$ such that it is an algebraic extension of E, and any polynomial with cofficients in $E$ or $\bar{E}$ has a zero in $\bar{E}$

### Fundamental theorem of algebra

The complex field $\mathbb{C}$ is algebraically closed


## Algebraic integers

A complex number $\alpha$ is an *algebraic integer* if it is the zero of a
monic polynomial with integer coefficients.

If $\alpha \in \mathbb{C}$ is an algebraic number, then $n\alpha$ is an
algebraic integer for some integer $n$

## Construction with straightedge and compass

- The set of constructible numbers $K$ is a subfield of $\mathbb{R}$, i.e. $K \leq \mathbb{R}$
- $[K:\mathbb{Q}] = \infty$
- For a $u \in K$, $[\mathbb{Q(u)}:\mathbb{Q}] = 2^n$ for some $n$
- $u$ is constructible if there is a finite chain of simple quadradic radical extensions, $\mathbb{Q} \leq \mathbb{\sqrt{\alpha}} \leq \mathbb{\sqrt{\alpha_2}} ... \leq \mathbb{\sqrt{\alpha_n}} = \mathbb{Q}(u)$



# Lecture 15

## Finite fields

If a field $F$ is finite, then $char(F) = p$ where $p$ is a prime number and $F$ has $p^n$ elements.

If a polynomial $f(x) \in \mathbb{Z}_p$ over a finite field is irreducible and of degree $n$, then
$$
\frac{\mathbb{Z}_p[x]}{(f(x))}
$$
is a finite field with $p^n$ elements


Fir any prime $p$ and positive integer $n$, there exists some irreducible polynomial $f(x) \in \mathbb{Z}_p[x]$ which has degree $n$

**Corollary**: For any prime power $p^n$ there exists a finite field of $p^n$
elements

## The frobenius endomorphism

If $F$ is a field with characteristic $p$, then
$$
\varphi : F \rightarrow F
$$
$$
\varphi(v) = v^p
$$
is an injective field homomorphism.

If $F$ is finite, then it is an isopmorphism

### Fixed field of the Frobenius endomorphism

If $F$ is a field with characteristic $p$ which is an algebraic extension of its prime field $\mathbb{Z}_p$, then $\mathbb{Z}_p$ is exactly the fixed set of the endomorphism, i.e.

$$
\mathbb{Z}_p = \{u \in F | \varphi(u) = u\}
$$

## Polynomial separability

A polynomial $f(x) \in F[x]$ where $F$ is a field is separable if it has
$\text{deg}(f)$ distinct zeroes in its splitting field, i.e. no multiple zeros.

An algebraic extension $F \leq L$ is separable if every element in $L$ is the zero of a separable polynomial in $F[x]$

$f(x)$ is only separable in if $\text{gcd}(f(x), f'(x))=1$

## Uniqueness and existence of finite fields

For any prime power $q = p^n$, there is a finite field $F$ with $q$ elements. Any such field is isomorphic to the splitting field of
$$
\phi_q(x) = x^q - x \in \mathbb{Z}_p[x]
$$


## Galois field
The unique up to isomorphism field with $q = p^n$ elements is denoted $\text{GF}(q)$ and is called the *galois field* of order $q$


## Finite field properties

If $F$ is a field and $G$ is a finite subgroup of the multiplicative group
$F^*$, then $G$ is cyclic

If $F$ is finite with characteristic $P$, then $F^* = \langle u \rangle$ for
some $u \in F^*$, and $F = \mathbb{Z}_p(u)$

** NOTE ** Peoperties of finite fields is incomplete


