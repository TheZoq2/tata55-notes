from sage.all import *

##
s6 = PermutationGroup(['()', '(1,2)', '(3, 4)', '(1,2)(3,4)'])

print("Subgroups")
print(s6.conjugacy_classes_subgroups())
##
