from sage import *
from sympy import *
import sympy as sp
import sympy

from sage.plot.point import Point

##

def q_1_2():
    Z2 = GF(2)
    R.<x> = Z2[]
    f=x^3+x+1
    f.factor()

q_1_2()

##

def q_1_3():
    Z3 = GF(3)
    R.<x> = Z3[]

    for c in range(0, 3):
        print(f"c: {c}")
        f=x^3 + c*x^2 + +1
        display(f.factor())

q_1_3()

##

def q_1_2():
    Z2 = GF(2)
    R.<x> = Z2[]
    f=x^3+x+1
    f.factor()

q_1_2()

##

def q_2_2_part1():
    p = sp.symbols('p')
    display(sp.expand((p-1)^4))
    display(sp.expand((p-1)^3))



def q2_2():
    for p in [p for p in range(2, 20) if is_prime(p)]:
        Zp = GF(p)
        R.<x> = Zp[]
        f=x**4 + x**3 + x + 1
        print(f.is_irreducible())

        print(f"Z_{p}")
        # for x in range(0, p):
        x = p-1
        if (x**4 + x**3 + x + 1) % p == 0:
            print(f"{x} is a zero")

q_2_2_part1()
q2_2()

##

def q_2_3():
    for p in [p for p in range(2, 100) if is_prime(p)]:
        Zp = GF(p)
        R.<x> = Zp[]
        f=x^4+2*x+2

        if f.is_irreducible():
            print(f"{p}, ", end="")
    print()

    for p in [p for p in range(2, 100) if is_prime(p)]:
        Zp = GF(p)
        R.<x> = Zp[]
        f=x^4+2*x+2

        if f.is_irreducible():
            print(f"{p}:", end='')
            for n in range(2,30):
                GFn = GF(p^n)
                f_n = f.change_ring(GFn)

                if f_n.is_irreducible():
                    print(f"{p}^{n}, ", end="")
            print()


q_2_3()

##

def q_2_6():
    alpha = 2*b*sqrt(m)

q_2_6()
##

def q_2_8_1():
    x = symbols('x')

    poly = x^3 - 3*x - 1

    display(sympy.expand(poly.subs({'x': x+1})))
    display(sympy.expand(x-1)^3)

    x_bar = symbols(r'\bar{x}')

    g = (x-x_bar)
    subpoly = sympy.div(poly, g)[0]
    display(sympy.div(poly, g)[1])

    display(subpoly.factor())

q_2_8_1()

##

def q_2_8_2():
    x = symbols('x')

    poly = x^3 - 3*x - 2

    factor1 = (x-2)
    g = sympy.div(poly, factor1)[0]
    print("g")
    display(g)

    factor2 = (x+1)
    h = sympy.div(g, factor2)[0]
    print("h")
    display(h)

    display(expand(factor1 * factor2 * h))

q_2_8_2()

##

def q_2_8_3():
    (x, x_bar, y_bar) = symbols(('x', r'\bar{x}', r'\bar{y}'))

    g = x^2 - x_bar - 3 - x_bar^2

    display(g)
    display(factor(g))

    print("h(x):")
    h = div(g, (x-y_bar))[0]
    rem = div(g, (x-y_bar))[1]
    display(h)
    print("Remainder:")
    display(rem)

    display(expand((x-x_bar)*(x-y_bar)*h))


q_2_8_3()

##

# Example from lecture 14
def ex14():
    x,xbar,a,b,c = symbols(['x', r'\bar{x}', 'a', 'b', 'c'])

    g = x^2 + xbar*x + xbar^2
    display(g)

    g_replaced = expand(g.subs({x: a + b*xbar + c*xbar^2}).subs({xbar^3: 2}))

    display(g_replaced)

    # x = symbols('x')

    # poly = x^3 - 2

    # solution = solve(poly)

    # display(solution[0])
    # display(solution[1])
    # display(solution[2])


    # point_list = [[re(sol), im(sol)] for sol in solution]
    # return scatter_plot(point_list, marker='s').plot()

    # # display(poly.subs({'x': -1/2 + i * sqrt(2)/3}))

    # # poly2 = (x^3 - 2) / (x - cbrt(2))
    # # display(poly2)

ex14()
