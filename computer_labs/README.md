# Running in vim

Install `https://github.com/jupyter-vim/jupyter-vim`

Install sage, jupyter and jupyter console~~

~~Open the jupyter notebook `sage -n lab1.ipynb`~~

~~Open a jupyter console using `jupyter console --existing`~~
Open a jupyter console using `jupyter qtconsole --kernel sagemath`

Connect to the console in vim
```
:JupyterConnect
```

Run cells using
```
:JupyterSendCell
```
