##

from collections import defaultdict
from sage.all import *
from sage.combinat.combination import ChooseNK

##

def graph_iso(n: int):
    G = libgap.SymmetricGroup(n)
    E = Subsets([1..n],2)
    GR = Subsets(E)
    # print(GR)
    GRE=[sorted(list([sorted(list(f)) for f in e])) for e in sorted(GR)]
    # print(GRE)
    graphsiso=G.Orbits(GRE,libgap.OnSetsSets)
    for gr in graphsiso:
        print(f"Edges: {len(gr[0])}. Class size {len(gr)}. Repr: {gr[0]}")
        # print('representant:',gr[0])
        # print()

def q1():
    for n in range(2, 7):
        print(n)
        graph_iso(n)
        print()
q1()

##

# Using the original lab code as a sanity check for my modifications
def k_colorings_orig(n, k_):
    a = lambda s,x: {s(x[0]),s(x[1])}
                               
    N = binomial(n,2)
    SUBn = sorted(Subsets(n,2))
    Sn = SymmetricGroup(n)

    #for g in Sn:

    Hn = PermutationGroup(
        [Permutation([SUBn.index(a(g,p))+1 for p in SUBn]) 
         for g in Sn])    
       
    CycleIndexHn = Hn.cycle_index()    
    R=PolynomialRing(QQ,'x',N)

    SS= sum([(i[1]*prod([R.gens()[j-1] for j in i[0]])) for i in CycleIndexHn])
    R1.<k>= PolynomialRing(QQ)
    f1 = R.hom([k for j in range(N)],R1)
    GF1 = f1(SS)
    return GF1(k_)

def k_colorings(n, k):
    # Applies the transformation s to the first 2 (presumably all) elements x
    # of the pair x
    # Presumably this applies an action on G to (G, G)
    a = lambda s,x: {s(x[0]),s(x[1])}

    # Computes the binomialcoefficient (n choose 2)
    N = binomial(n,2)
    # Returns the subsets of size2 of {1..n} and sorts them. This is effectively
    # all pairs of (x, y) where x,y 1..n
    SUBn = sorted(Subsets(n,2))
    Sn = SymmetricGroup(n)

    # For each member in S_n, i.e. each permutation of the members of N,
    # permute the indices and compute which edge gets mapped to which other
    # edge.
    Hn = PermutationGroup(
        [Permutation([SUBn.index(a(g,p))+1 for p in SUBn]) for g in Sn])


    # Now we have the permutations on the edges which we get by permuting the vertices

    # The rest of the code follows the colorings part of lecture 8. The next few statements
    # generate the cycle index polynomial

    # Computes the cycle index of the permutations on the edges
    CycleIndexHn = Hn.cycle_index()
    # print("Cycle index: ", CycleIndexHn)

    # print("Cycle types: ", [i for i in CycleIndexHn])
    # print("R.gens", R.gens())

    # The next 2 lines builds the cycle index polynomial by, for each (cycle_type, scale),
    """
    # Create the ring of real numbered polynomials with N (n choose 2) variables
    R=PolynomialRing(QQ,'x',N)

    ci_polynomial = sum([(scale*prod([R.gens()[j-1] for j in cycle_types])) for (cycle_types, scale) in CycleIndexHn])
    """
    # for example ([1,1,2], 1/10) replacing the cycle types with the corresponding variable
    # in the polynomial, i.e. `1 -> x_0`, `2 -> x_1` and so on.
    #
    # The rest of the code from the lab, i.e. the following
    """
    print("ci_polynomial: ", ci_polynomial)
    R1.<k> = PolynomialRing(QQ)
    f1 = R.hom([k for j in range(N)],R1)
    GF1 = f1(ci_polynomial)
    print(GF1)
    print(GF1(2))
    """
    # somehow, with some weird sage magic, replaces all x_n variables with
    # k in order to get a polynomial that gives the number of k-colorings.
    # However, this step seems a bit complex, so I decided to just do it all in
    # one pass, and compute the cycle index polynomial, with x_n replaced with
    # the k passed as a function parameter directly.

    ci_polynomial_k = sum([(scale*prod([k for _ in cycle_types])) for (cycle_types, scale) in CycleIndexHn])
    # print(ci_polynomial_k)
    return ci_polynomial_k

    # There are probably some performance implications of this, once we have
    # the polynomial given by k, we can quickly get the results for multiple
    # k-values, but my solution here requries some re-computations for all of
    # them. But, performance isn't important here, at least not nearly as
    # important as me being able to explain the code :)
    # (Also, when running both, they are both about equally slow, so maybe that theory
    # doesn't work)

def q2(result_fn):
    print("n  ", end="")
    [print(f"k={k:<10}", end="") for k in range(2,10)]
    print("")
    for n in range(2, 7):
        print(f"{n}: ", end="")
        for k in range(2, 10):
            print(f"{int(result_fn(n ,k)):<12}", end = "")
        print("")


print("Original")
q2(k_colorings_orig)
print("Modified")
q2(k_colorings)

##
def n_choose_k(n, k):
    return factorial(n)/(factorial(k) * factorial(n-k))

def compute_orbits(n):
    GS = CyclicPermutationGroup(n)
    G = GS.gap()
    # Compute the number of inscribed triangles
    E = Subsets([1..n],3)


    GRE=[sorted(list(e)) for e in sorted(E)]
    triangles=G.Orbits(GRE,libgap.OnSets)
    for gr in triangles:
        print(" Representative size: ", len(gr[0])," orbit size: ", len(gr), " repr: ", gr[0])

    num_orbits = len(triangles)
    if n % 3 == 0:
        computed_num = (n_choose_k(n, 3) - n/3) / (n) + 1
    else:
        computed_num = n_choose_k(n, 3) / n
    print(f"Count: {num_orbits}, computed {computed_num}")

def q3():
    for i in range(4, 16):
        print(f"{i}")
        compute_orbits(i)


    # print("27 as a sanity check:")
    # compute_orbits(27)

q3()

##

def q4():
    a4 = AlternatingGroup(4)

    display(a4)
    for g in a4:
        cclass = a4.conjugacy_class(g).list()
        cclass.sort()
        print(f"{g}: {cclass}")


q4()


##
def q5():
    s4 = SymmetricGroup(4)

    print("S_4 commuters: ")
    x = s4((1, 2, 3, 4))
    for g in s4:
        # print(f"{g}, {x*g}");
        if g*x == x*g:
            print(f"{g.cycle_string()} commutes with {x}")

    print()

    # Verify my result
    for n in range(4, 10):
        commuters = 0
        sn = SymmetricGroup(n)
        x = sn((1, 2, 3, 4))
        for g in sn:
            # print(f"{g}, {x*g}");
            if g*x == x*g:
                commuters += 1

        expected = 4 * factorial(n-4)
        print(f"{n}: computed: {commuters} expects: {expected}")



q5()

##

def q6():
    d6 = DihedralGroup(6)

    for sub in d6.subgroups():
        card = sub.cardinality()
        if card == 4 or card == 3:
            print(sub)

q6()

##
def check_sylow_subgroup_uniqueness():
    for n in range(1, 10):
        dn = DihedralGroup(n)
        print(f"{n}: {dn.cardinality()}")

        sylow_subgroup_candidates = defaultdict(list)

        for sub in dn.subgroups():
            card = sub.cardinality()
            divisors = list(factor(card))

            # This is potentially a sylow subgroup
            if len(divisors) == 1:
                sylow_subgroup_candidates[divisors[0][0]].append(divisors[0])

        for (pow, candidates) in sylow_subgroup_candidates.items():
            if pow <= 2:
                continue

            max_k = max(candidates, key = lambda x: x[1])

            final_candidate = list(filter(lambda x: x[1] == max_k[1], candidates))
            if len(final_candidate) != 1:
                print("Non-unique sylow p-group")

            print(f"{pow}: {candidates} -> {final_candidate}")
            # print(f"{pow}: {max_k}")

def q7():
    check_sylow_subgroup_uniqueness()

q7()

