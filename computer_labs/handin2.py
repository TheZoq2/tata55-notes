#
from typing import List
from sage.all import *
import functools

## Utilities

def count_orders(max_n, group):
    counts = [0]*max_n
    for g in group:
        o = order(g)
        if o <= max_n:
            counts[o-1] += 1

    return counts


def tabulate_f(max_n, counter):
    print(" g  : ", end="")
    for i in range(1, max_n + 1):
        print(f"{i:>2} ", end="")
    print()
    for n in range(1, max_n + 1):
        orders = counter(max_n, n)

        print(f"[{n:>2}]: ", end = "")
        for order in orders:
            if order != 0:
                print(f"{order:>2} ", end = "")
            else:
                print(" . ", end = "")
        print()

## 1)



def q1():
    def compute_orders(max_n, n):
        def inner(k):
            if n % k == 0:
                return euler_phi(k)
            else:
                return 0

        return list(map(inner, range(1, max_n+1)))

    tabulate_f(12, lambda max_n, n: count_orders(max_n, CyclicPermutationGroup(n)))
    print()
    tabulate_f(12, compute_orders)

q1()


## 2) Dihedral group question

def q2():
    def compute_orders(max_n, n):
        counts = [0]*max_n

        # Reflections
        counts[1] += n

        # Rotations by `r/n * 360`. To make the code simpler, skip r=0 and
        # use r=n instead
        for r in range(1, n+1):
            o = n / gcd(n, r)
            counts[o-1] += 1


        return counts

    tabulate_f(12, lambda max_n, n: count_orders(max_n, DihedralGroup(n)))
    print("")
    tabulate_f(12, lambda max_n, n: compute_orders(max_n, n))

q2()

## 3)

def q3():
    # Computes lcm(lcm(lcm(...))) of a list of integers
    def cross_lcm(inputs):
        return functools.reduce(lambda x, y: lcm(x, y), inputs[1:], inputs[0])


    # Returns the cycle types possible in S_n wher min is the minimum length of the
    # cycle i.e. cycle_types(6, 2) will return [[2, 2 2], [3,3], [6]]
    # When called with cycle_types(n, 1) it returns *all* cycle types of S_n
    def cycle_types(n: int, max_type: int) -> List[List[int]]:
        if n == 0:
            return [[]]
        result = []
        for k in range(1, min(max_type, n) + 1):
            # If this is a perfect fit
            rest = cycle_types(n-k, k)
            for type in rest:
                result += [[k] + type]
        return result


    def final_answer(max_n, n):
        counts = [0]*max_n*2
        for ct in cycle_types(n, n):
            # Compute which bin this falls into (lcm(cycle_types))
            k = cross_lcm(ct)

            cycle_counts = {}
            for c in ct:
                if not c in cycle_counts.keys():
                    cycle_counts[c] = 0
                cycle_counts[c] += 1

            this_count = factorial(n) / product(map(
                lambda c: c[0]**c[1] * factorial(c[1]),
                cycle_counts.items()
            ))
            # Magic -1 for ~~good luck~~ to go between 0 and 1 indexing
            counts[k-1] += int(this_count)
        return counts




    print("Computed by sage")
    tabulate_f(6, lambda max_n, n: count_orders(max_n, SymmetricGroup(n)))
    print("")
    print("Final answer; ")
    tabulate_f(6, final_answer)

    print(cycle_types(5, 5))

q3()

# LINK to stack exchange post: 
# https://math.stackexchange.com/questions/1340297/finding-the-number-of-elements-of-particular-order-in-the-symmetric-group/1340368#1340368

# Output:
# Computed by sage
#  g  :  1  2  3  4  5  6 
# [ 1]:  1  .  .  .  .  . 
# [ 2]:  1  1  .  .  .  . 
# [ 3]:  1  3  2  .  .  . 
# [ 4]:  1  9  8  6  .  . 
# [ 5]:  1 25 20 30 24 20 
# [ 6]:  1 75 80 180 144 240 
# 
# Final answer; 
#  g  :  1  2  3  4  5  6 
# [ 1]:  1  .  .  .  .  .  .  .  .  .  .  . 
# [ 2]:  1  1  .  .  .  .  .  .  .  .  .  . 
# [ 3]:  1  3  2  .  .  .  .  .  .  .  .  . 
# [ 4]:  1  9  8  6  .  .  .  .  .  .  .  . 
# [ 5]:  1 25 20 30 24 20  .  .  .  .  .  . 
# [ 6]:  1 75 80 180 144 240  .  .  .  .  .  .


## 4)

def q4():
    # How many inversions?
    from collections import Counter
    G=SymmetricGroup(8)
    numinvlist = [Permutation(g).number_of_inversions() for g in G]
    co=Counter(numinvlist)
    print(add([co[_] for _ in range(0,12,2)]))

    scale = 38500
    mean = 14
    var = 4
    normal_dist = list(map(lambda x: scale * 1/(var * sqrt(2 * pi)) * exp(-1/2 * ((x - mean)/var)**2), range(0,28)))

    (list_plot(co) + list_plot(normal_dist, rgbcolor='red')).show()

q4()

## 5)

P = polytopes.dodecahedron()

reprs = G.conjugacy_classes_representatives()

for repr in reprs:
    print(f"{order(repr)}: {repr}")

print()

for repr in reprs:
    print(f"{repr}")
    # If this is a rotation which has enve order, it might result in a larger
    # rotation around the same axis. See if we can compute that 
    compound = repr
    for o in range(1, order(repr)):
        compound = compound * repr

        if compound in reprs:
            matched.append(compound)

            print(f"    ^{o} produces {compound}")

##

colors = ["a", "b", "c"]

colors
##



