from sage.all import *


##
# Plot the Hasse diagram of all positive divisors of 360
P=posets.DivisorLattice(360)
P.show()
##
# Then Plot the intervall between 4 and 360
P.subposet( P.order_filter([4]) ).show()
##


# Plot the Hasse diagram of all positive divisors of 360
P=posets.DivisorLattice(90)
P.show()

##

## GCD of 98765 and 12345, and as a linear combination

a, b = (98765, 12345)
(d, x, y) = xgcd(a, b)

print(d, a*x, b*y)

##

a = 1233
n = 3237
d,x,y = xgcd(a,n)

##

# Find the multiplicative inverse of a mod n, check
a = 1273
n = 3237
d,x,y = xgcd(a,n)
print(d,x,y)
print(d.divides(1))
print(a*x % n)

##
a = 3
b = 5
n = 11

x = None
var('x')
var('b')

print("x: ", x)
solve_mod([b == a*x], n, solution_dict=True)

##

# Which congruences mod 19 have square roots?

var('a')
solve_mod([x^2 == a], 19, solution_dict=True)

## Mod which primes does 19 have a square root

p_10 = [nth_prime(i) for i in range(1, 10)]

for p in p_10:
    print(p)

    var('a')
    print(solve_mod([x^2 == 19], p, solution_dict=True))
##


crt([112, 115], [13, 97])

##

# Exercise CRT

def f_crt(k):
    var('x')
    p_k = nth_prime(k)
    prod = product([nth_prime(i) for i in range(1, k)])
    return solve_mod([x == -k], p_k)[0][0] / prod

sol = [f_crt(k) for k in range(1, 100)]

print(sol)
list_plot(sol)


##

from collections import Counter

n = 8
Zn = Integers(n)
# Zn.addition_table(names = 'elements')

orderlist = [a.order() for a in Zn]
co = Counter(orderlist)
print(sorted(co.items()))
