from sage import *

##

def q_1_1():
    var('x')
    Z2 = GF(2)
    R.<x> = Z2[]
    f = R(x^5 + x^3 + 1)

    K = R.quotient_by_principal_ideal(ideal(f))
    display(K)
    print("i) part 1, is field: ", K.is_field())

    f_k = f.change_ring(K)
    display(f_k.factor())


q_1_1()


# TODO: ii

##


def q_1_2():
    # Original polynomial in Z3
    Z3 = GF(2)
    R.<x> = Z3[]
    f=x^3-x+1

    # No factors => irreducible
    display(f.factor())

    # Compute splitting field, verify that it factors now
    K = f.splitting_field('u')
    display(K)
    f_k = f.change_ring(K)
    display(f_k.factor())

    roots = f_k.roots()
    display(roots)
    r = [r[0] for r in roots]

    R.<c0, c1, c2> = K[]
    display(R)

    I = ideal(
        c0*r[0]^0 + c1*r[1]^0 + c2*r[2]^0 - 1,
        c0*r[0]^1 + c1*r[1]^1 + c2*r[2]^1,
        c0*r[0]^2 + c1*r[1]^2 + c2*r[2]^2,
    )
    display(I)
    display(I.variety())

    c_actual = [I.variety()[0][c] for c in ['c0', 'c1', 'c2']]
    display(c_actual)

    seq = [(n, sum([c_actual[i]*r[i]^n for i in range(0,3)])) for n in range(20)]
    display(seq)


    mul_gen = K.multiplicative_generator()

    # Compute the order by generating the group from its generator
    order = 1
    Kstar = mul_gen
    while Kstar*mul_gen != mul_gen:
        order += 1
        Kstar = Kstar * mul_gen

    display(order)

    # 7 == 7, it does divide the order

q_1_2()
